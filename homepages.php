<?php
	/*
	 * Create the homepage and indexed pages after it.
	*/

	//// Config ////

	$entriesPerPage = 15;
	//$homepageEntries = $entriesPerPage;
	$homepageEntries = 10;

	$searchDirectories = array(
		"blogposts",
		"smallchat",
	);

	$homepagePattern = 'public_html/page/%03d';
	$fullpagePattern = 'public_html/blog/%05d';
	$fullpageReadMore = '/blog/%05d/';

	$subpageTitlePattern = "Posts - Page %d | Ryan Leonard";

	////  Code  ////

	$files = array();

	function getTime($filename) {
		include($filename);
		return DateTime::createFromFormat("m/d/Y (H:i)", $time)->getTimestamp();
	}

	foreach ($searchDirectories as $directory) {
		echo "Processing $directory\n";
		foreach (glob("{$directory}/*.php") as $filename) {
			echo "Found $filename\n";
			$files[getTime($filename)] = $filename;
		}
	}

	function loadFile($filename, $readMoreLink) {
		include($filename);
		include("templates/blog-".$template.".php"); // TODO: add stuff to template
		return array($short, $long);
	}

	$page = 1;
	$entry = 0;
	$summaries = array();

	foreach ($files as $modificationTime => $filename) {
		echo "Processing $filename\n";
		list($short, $long) = loadFile($filename, sprintf($fullpageReadMore, $entry));
		$fullDir = sprintf($fullpagePattern, $entry);
		echo "Creating directory $fullDir\n";
		mkdir($fullDir, 0777, true);
		file_put_contents("{$fullDir}/index.php", $long);
		$entry++;
		$summaries[] = $short;
		if($entry == $homepageEntries) {
			$homepageWritten = true;
			writeHomepage(implode("", $summaries), $fullDir);
		}
		//elseif($entries - $homepageEntries > 0 && ($entries - $homepageEntries) % $entriesPerPage = 0) {
		elseif($entry - $homepageEntries > 0 && ($entry - $homepageEntries) % $entriesPerPage = 0) {
			writePage(implode("", $summaries), $fullDir, $page);
			$page++;
		}
	}

	if(!isset($homepageWritten)) {
		writeHome(implode("", $summaries), true);
	}

	echo "Success.\n";

	function writeHome($short, $fullDir) {
		echo "Writing homepage.\n";
		include("public_html/index.template.php");
		$contents .= "\n".$short;
		include("templates/".$template.".php");
		file_put_contents("public_html/index.html", $page);
	}

	function writePage($contents, $fullDir, $pageIndex) {
		global $subpageTitlePattern, $homepagePattern;
		$template = "main";
		$title = sprintf($subpageTitlePattern, $pageIndex);
		$menuItem = "/";
		include("templates/".$template.".php");
		$url = sprintf($homepagePattern, $pageIndex);
		file_put_contents($url, $page);
	}