SOURCES=$(shell find . -name "*.template.php" | sed 's/.template.php/.html/')

all : $(SOURCES)
	php homepages.php

GENERATED=$(shell find public_html -name "*.html" 2>&1 | grep -v 'template')

TEMPLATES=$(shell find templates)

%.html: %.template.php %.template.html $(TEMPLATES)
	php template.php $< $@

clean:
	rm -f $(GENERATED)