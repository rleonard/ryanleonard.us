<?php
	/*
		ryanleonard.us/projects/index.template.php -> ryanleonard.us/projects/index.html
		Template class: main (/templates/main.php)
		Description:
			Basic homepage.  Uses external file for contents.
	*/
	$template = "main";
	$title = "Tech Task Force | Ryan Leonard";
	$menuItem = "/ttf/";
	$contents = file_get_contents("public_html/ttf/index.template.html");