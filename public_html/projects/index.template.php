<?php
	/*
		ryanleonard.us/projects/index.template.php -> ryanleonard.us/projects/index.html
		Template class: main (/templates/main.php)
		Description:
			Basic homepage.  Uses external file for contents.
	*/
	$template = "main";
	$title = "Projects | Ryan Leonard";
	$menuItem = "/projects/";
	$contents = file_get_contents("public_html/projects/index.template.html");