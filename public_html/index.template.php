<?php
	/*
		ryanleonard.us/index.template.php -> ryanleonard.us/index.html
		Template class: main (/templates/main.php)
		Description:
			Basic homepage.  Uses external file for contents.
	*/
	$template = "main";
	$title = "Home | Ryan Leonard";
	$menuItem = "/";
	$contents = file_get_contents("public_html/index.template.html");