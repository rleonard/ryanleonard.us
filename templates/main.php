<?php
	/*
		/templates/main.php
		Basic (main) template for website
		File directory: /templates/main
	*/
	/*
		Check various page variables, and set them to defaults if they haven't been defined.
		Useful to add extra libraries.
	*/
	$headAppend = isset($headAppend) ? $headAppend : "";
	$scriptFooter = isset($scriptFooter) ? $scriptFooter : "";
	if(!isset($cssLibraries)) {
		$cssLibraries = "";
	}
	$cssLibraries .= <<<EOD
		<link href="/lib/bootstrap/theme/united.css" rel="stylesheet" />
		<link href="/lib/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" />
EOD;
	/*
		Add extra external template resources.
	*/
	include("main/header.php");
	/*
		Define entire page.
	*/
	$page = <<<EOD
<!DOCTYPE html>
<html>
	<head>
		<title>$title</title>
$cssLibraries
$headAppend
	</head>
	<body style="padding-top: 50px;">
$header
$contents
		<script src="/lib/jquery/jquery.js"></script>
		<script src="/lib/bootstrap/js/bootstrap.min.js"></script>
		$scriptFooter
	</body>
</html>
EOD;
