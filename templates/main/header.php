<?php
	$items = array(
		array("url"=>"/", "name"=>"Home"),
		array("url"=>"/projects/", "name"=>"Projects"),
		array("url"=>"/ttf/", "name"=>"Tech Task Force"),
	);
	$header = <<<EOD
		<div class="navbar navbar-fixed-top">
			<div class="navbar-inner">
				<a class="brand" href="/">Ryan Leonard</a>
				<ul class="nav">\n
EOD;
	foreach($items as $link) {
		$isSelected = isset($menuItem) && $link["url"] == $menuItem;
		$selected = $isSelected ? ' class="active"' : "";
		$url = $link["url"];
		$name = $link["name"];
		$header .= <<<EOD
					<li{$selected}><a href="$url">$name</a></li>\n
EOD;
	}

	$header .= <<<EOD
				</ul>
			</div>
		</div>
EOD;
