<?php
	/*
		/templates/blog-main.php
		Basic (main) template for blog entries on the website
		File directory: /templates/main
	*/

	switch ($contentType) {
		case 'markdown':
			include("main/Parsedown.php");
			$Parsedown = new Parsedown();
			$shortText = $Parsedown->text($shortText);
			if(isset($longText)) {
				$longText = $Parsedown->text($longText);
			}
			break;
		
		default:
			$shortText = $shortText;
			if(isset($longText)) {
				$longText = $longText;
			}
			break;
	}

	$safeTitle = str_replace(" ", "_", $title);

	if(isset($longText) && isset($readMoreLink)) {
		$readMore = <<<EOD
<a href="$readMoreLink?/$safeTitle" class="btn btn-primary">Read More</a>
EOD;
	}

	$short = <<<EOD
		<div class="container-fluid">
			<div class="row">
				<div class="span12">
					$shortText
					<p class="pull-right">
						$readMore
					</p>
				</div>
			</div>
		</div>
EOD;

	/*
		Check various page variables, and set them to defaults if they haven't been defined.
		Useful to add extra libraries.
	*/
	$headAppend = isset($headAppend) ? $headAppend : "";
	$scriptFooter = isset($scriptFooter) ? $scriptFooter : "";
	if(!isset($cssLibraries)) {
		$cssLibraries = "";
	}
	$cssLibraries .= <<<EOD
		<link href="/lib/bootstrap/theme/united.css" rel="stylesheet" />
		<link href="/lib/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" />
EOD;
	/*
		Add extra external template resources.
	*/
	include("main/header.php");
	/*
		Define entire page.
	*/
	$long = <<<EOD
<!DOCTYPE html>
<html>
	<head>
		<title>$title</title>
$cssLibraries
$headAppend
	</head>
	<body style="padding-top: 50px;">
$header
$longText
		<script src="/lib/jquery/jquery.js"></script>
		<script src="/lib/bootstrap/js/bootstrap.min.js"></script>
		$scriptFooter
	</body>
</html>
EOD;
