<?php
	/*
		ryanleonard.us/blogposts/techtaskforce-items.php -> ryanleonard.us/blog/[0-9]+/?/Tech_Task_Force_Projects/
		Template class: main (/templates/main.php)
		Description:
			Basic homepage.  Uses external file for contents.
	*/
	$template = "main";
	$title = "Tech Task Force Projects";
	$updated = false;
	$time = "05/20/2014 (17:19)";
	$contentType = "markdown";
	$shortText = file_get_contents("blogposts/techtaskforce-items.short.md");
	$longText = file_get_contents("blogposts/techtaskforce-items.long.md");