I finally got around to [listing](/ttf/) all of the various projects I worked on during my first year on the Tech Task Force.  
Wow.

I figured now that school has ended, I might take a minute to reflect on my first year in the Tech Task Force.

For anyone unfamiliar with the Tech Task Force, (Mr.) [Andrew Marcinek](http://www.andrewmarcinek.com/) brought some of
his ideas from Burlington to Groton-Dunstable, and reimplemented his [Student Help Desk](http://bhshelpdesk.com/) as the
"Tech Task Force".  I was the first to schedule a semester of the course, and roped in one of my best friends,
John DeNyse during the first few weeks of school.

The Tech Task Force started as a low-level IT/technology integration position.  The first things we did were migrate
email from First Class to a new Google Apps for Education setup that was rolled out to the school district, and we also
inventoried the technology status of rooms and upgraded Apple TVs.

We soon started helping (Mrs.) [Audra Kaplan](https://sites.google.com/a/gdrsd.org/audralkaplan/), who were were assigned to,
with her [Tech Tuesdays and Thursdays](https://sites.google.com/a/gdrsd.org/audralkaplan/tech-tuesday-thursday-agenda), where we
could help teachers one-on-one if they needed help with a problem, freeing Mrs. Kaplan up to teach a group.

We also taught larger classes when they needed help with new technologies being tried out in the district, such as setting up
classes to collect data via Google Forms, or setup a standard layout of folders on Google Drive to ease the clutter for teachers.

My favorite moment of the year, though, was inventorying the Chromebooks.  The day started like any other.  We had already unboxed and enrolled the 600 Chromebooks that came to the High School, but now we had to grab the serial numbers, district tag, and a few other details from each machine.  The serial numbers are a little hard to read, causing us to squint to make out the numbers.  Timeout.  I see a barcode right above the serial number.  Could it be?  Yes, a quick scan using an Android reveals that the hexadecimal serial number is encoded in the barcode.

I figure that it wouldn't take more than 30 minutes to code a junky scanning program in Python for my Android phone, and 45 to 50 minutes later, I've got a scanning application done, and I was chugging away through my first cart when Mr. Marcinek [walked in](http://www.andrewmarcinek.com/2013/09/gobsmacked.html).

This was the first time I had really been able to use code during the Tech Task Force.
I made sure it wasn't the last.

Soon we had a few projects going, including a program to [manage district PR](https://bitbucket.org/rleonard/osid-server), and
a plan for a Google Drive plugin.  All this coding made the highlight of each day instead the highlight of the year.

Along with the coding projects, I continued to install Apple TVs (I became quite comforatable in the ceilings and found easier ways to sprint up stairs carrying a ladder), aiding teachers and classrooms.  I also gave the school Technovation Programming team a hand with teaching programming, and never left, and they eventually got used to putting up with me enough to make me a mentor.

This is a great list of what we helped the school with in a year.  I got a lot out of helping the school with technology.
But what I got most out of the Tech Task Force is very selfish.  I live for the cheers of "Lenny" when I get up to fix a projector
in a class-wide assembly.  I live for the problem solving and quick thinking needed when I'm towering over a class on a ladder trying to install an Apple TV.  I live for the friends I have made helping students from the Java class on their app.  I live for hearing "Huh.  That's an interesting idea" when I propose a solution that nobody else has thought of.

All of these things can be found in the Tech Task Force.  It's not just a help desk.  I don't sit around doing menial tasks.
This is why I love the Tech Task Force.  I love it enough that I can't imagine it disappearing after I graduate, so I am
cold-calling any recommended students in lower grades who might take my place.
I happily signed up to continue working into the summer, and to take two semesters of the Tech Task Force in my senior year.

I don't know what things we will do next year, but that's why I love the Tech Task Force.
I am sure that we will find something that hasn't been done before, and have fun doing it.